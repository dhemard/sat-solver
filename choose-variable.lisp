(defun choose-variable (cnf)
  (if (symbolp cnf)
    cnf
    (cond ((member 'and cnf) (choose-variable (rest cnf)))
      ((member 'or cnf) (choose-variable (rest cnf)))
      ((member 'not cnf) (choose-variable (rest cnf)))
      (t (choose-variable (first cnf))))))