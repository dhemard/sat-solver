## Satisfiability Solver written in LISP

**To Run:**
- Install a version of LISP with a compiler
    - for example [GNU CLISP](https://clisp.sourceforge.io)
- In a terminal emulator
    - Clone the repository
    - In the root directory compile and run `sat-solver.lisp`
        - using CLISP:
        
            ```bash
            $ clisp sat-solver.lisp
            ```
    - The terminal outputs the results of the test cases included in `sat-solver.lisp`
- The test consists of a series of 14 Logical Expressions which are either **satisfiable** or **not satisfiable**
    - An expression is **satisfiable** if there exists some combination of values assigned to its variables, which restults in the entire expression evaluating to "`T`" meaning *true*
    - If the expression is **not satisfiable** it should evaluate to "`nil`" meaning *false*
- The purpose of the cases are to test the correctness of the implementation of the function `satisfiable`, which this assignment challenged us to complete 
